rule all:
    input: "b_out.txt"

rule generate_some_input:
    output: "{your_pattern}.txt"
    shell:
        """
        echo "{wildcards.your_pattern}" > {output}
        """

# Will *NOT* work because the `in_pattern` can *not* be inferred
rule generate_some_output:
    input: "{pattern}_in.txt"
    output: "{pattern}_out.txt"
    message: "GENERATE PATTERNED {output} - Pattern: {wildcards.pattern}"
    shell:
        """
        cp {input} {output}
        """

