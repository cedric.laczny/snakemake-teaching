rule all:
    input: "b_out.txt"

rule generate_some_input:
    output: "{your_pattern}_in.txt"
    shell:
        """
        echo "{wildcards.your_pattern}" > {output}
        """

rule generate_some_output:
    input:
        first="{pattern}_first_in.txt",
        second="{pattern}_second_in.txt",
    output: "{pattern}_out.txt"
    message: "GENERATE PATTERNED {output} - Pattern: {wildcards.pattern}"
    shell:
        """
        cat {input.first} {input.second} > {output}
        """

