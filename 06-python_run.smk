NAMES = ["a", "b", "c"]
# at the top of the file
import os
import glob

print("These are the names: ")
print(NAMES)

rule all:
    input: "list_of_generated_output.txt"

rule generate_some_input:
    output: "{your_pattern}_in.txt"
    shell:
        """
        echo "{wildcards.your_pattern}" > {output}
        """

rule generate_some_output:
    input:
        first="{pattern}_first_in.txt",
        second="{pattern}_second_in.txt",
    output: "{pattern}_out.txt"
    message: "GENERATE PATTERNED {output} - Pattern: {wildcards.pattern}"
    shell:
        """
        cat {input.first} {input.second} > {output}
        """

# The `str(output)` is necessary as `open()` expectes a `string` type and not an `OutputFiles` type
rule list_generated_output:
    input: expand("{NAME}_out.txt", NAME=NAMES)
    output: "list_of_generated_output.txt"
    run:
        with open(str(output), 'w') as outfile:
            outfile.write('These are all the "out" names:\n')
            for book in glob.glob('*_out.txt'):
                outfile.write("%s\n" % book) 
