# Preparation
On the `gaia` cluster
```
oarsub -I
export PYTHONPATH=
ml lang/Python/3.6.4-foss-2018a
virtualenv venv
. venv/bin/activate
which python && python -V
pip install snakemake --no-cache-dir # Needed in case of my b0rked environment
```

# Basics
```
snakemake -s 01-basics.smk -n # Dry-run
snakemake -s 01-basics.smk -np # Dry-run and print the commands
snakemake -s 01-basics.smk -p # Generate the *default* target and print the commands
snakemake -s 01-basics.smk a.txt # Generate a *specific* target
snakemake -s 02-basics.smk a.txt # Print info message during run
```
## Missing rules - If your specified target does NOT exist
```
$ snakemake -s 01-basics.smk c.txt
Building DAG of jobs...
MissingRuleException:
No rule to produce c.txt (if you use input functions make sure that they don't raise unexpected exceptions).
```

# Wildcards - How to generalize your rules
## How NOT to
```
$ snakemake -s 03-wildcards_wrong.smk
Building DAG of jobs...
WildcardError in line 12 of /Users/cedric.laczny/Documents/snakemake-teaching/03-wildcards.smk:
Wildcards in input files cannot be determined from output files:
'in_pattern'
```

```
$ snakemake -s 03-wildcards_ambiguous.smk
Building DAG of jobs...
AmbiguousRuleException:
Rules generate_some_output and generate_some_input are ambiguous for the file b_out.txt.
Consider starting rule output with a unique prefix, constrain your wildcards, or use the ruleorder directive.
Wildcards:
	generate_some_output: pattern=b
	generate_some_input: your_pattern=b_out
Expected input files:
	generate_some_output: b_in.txt
	generate_some_input: Expected output files:
	generate_some_output: b_out.txt
	generate_some_input: b_out.txt
```

## How to
```
snakemake -s 03-wildcards_correct.smk
```

# Multiple inputs, multiple outputs
## Multiple *names* inputs
```
snakemake -s 04-multiple_input.smk
cat b_out.txt
```

## Multiple *named* outputs
```
snakemake -s 04-multiple_output-alternative.smk
cat b_first_out.txt
cat b_second_out.txt
```

Alternatively, but less clear to read
```
snakemake -s 04-multiple_output-alternative.smk
```

# Logs
```
snakemake -s 05-logs.smk
snakemake -s 05-logs.smk a_multilog.txt # For multiple logs
```

# Using Python inside
## Using `expand()`
Useful if *several* targets must be specified
```
snakemake -s 06-multiple_input_multiple_output_w_python.smk
lh
cat b_out.txt
```

## Using other Python commands
`print()`
```
snakemake -s 06-print_something.smk -p
snakemake -s 06-print_something.smk
```

## Multiple files
Use the `glob_wildcards()` function, s. https://hpc-carpentry.github.io/hpc-python/15-snakemake-python/
```
snakemake -s 06-python_run.smk
cat list_of_generated_output.txt
```

# Parallelization
## Simple parallelization
```
snakemake -s 07-threads.smk -p # Will use a *single* thread
cat list_of_generated_output.txt # All files should be in alphabetical order
snakemake --cores 2 -s 07-threads.smk -p # Will use *two* threads. Files can be listed *out*-of-order
```

## Cluster-scale parallelization
Demo using MUST/LeGeLiS sample

# Using a configuration file
```
snakemake -s 08-config_file.smk -p
cat list_of_generated_output.txt
snakemake --config names='["p", "q", "t"]' -s 08-config_file.smk -p # Overwriting via the CLI
snakemake --configfile config2.yaml -s 08-config_file.smk -p # Use a different configfile altogether
cat list_of_generated_output.txt
```

# MISSING
- Modularization by using `include`
- Using `conda` to define the toolchain

# Links
There are many other useful resources online on learning how to use `snakemake`.
Below is a non-comprehensive list:
- https://hackmd.io/7k6JKE07Q4aCgyNmKQJ8Iw?view by C. Titus Brown

